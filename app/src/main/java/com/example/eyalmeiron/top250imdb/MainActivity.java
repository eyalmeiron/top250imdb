package com.example.eyalmeiron.top250imdb;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static java.util.regex.Pattern.DOTALL;

public class MainActivity extends AppCompatActivity {

    public ImageView[] imgs;
    public String[] titles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        titles = new String[250];
        imgs = new ImageView[250];
        for (int i = 0; i < 250; i++) {
            imgs[i] = new ImageView(this);
        }

        Thread thread = new Thread(new Runnable() {

            OkHttpClient client = new OkHttpClient();

            String run(String url) throws IOException {
                Request request = new Request.Builder()
                        .url(url)
                        .build();

                try (Response response = client.newCall(request).execute()) {
                    return response.body().string();
                }
            }

            @Override
            public void run() {
                try {
                    int counter = 0;
                    System.out.println("started");
                    String html = run("http://www.imdb.com/chart/top");

                    Pattern pImg = Pattern.compile("<img src=\"(.*?)\" width=\"45\" height=\"67\"/>");
                    Matcher m = pImg.matcher(html);
                    while (m.find()) {
                        URL url = new URL(m.group(1));
                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                        imgs[counter].setImageBitmap(bmp);
                        counter++;
                    }

                    Pattern pTitle = Pattern.compile("<td class=\"titleColumn\">(.*?)>(.*?)</a>(.*?)</td>", DOTALL);
                    m = pTitle.matcher(html);
                    counter = 0;
                    while (m.find()) {
                        titles[counter] = m.group(2);
                        counter++;
                    }

                    System.out.println("DONE! found " + counter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();

        try {
            thread.join();
        } catch (InterruptedException e) {}

        LinearLayout ll_body = (LinearLayout) findViewById(R.id.ll_body);
        ll_body.removeAllViews();

        for (int i = 0; i < 250; i++) {
            LinearLayout ll_row = new LinearLayout(this);
            ll_row.setOrientation(LinearLayout.HORIZONTAL);
            ll_row.setTop(10);

            imgs[i].setMinimumWidth(45 * 3);
            imgs[i].setMinimumHeight(67 * 3);

            TextView textView = new TextView(this);
            textView.setText(titles[i].length() < 8 ? "  " + titles[i] : "  " + titles[i].substring(0, 7));
            textView.setTextSize(20);

            ll_row.addView(imgs[i]);
            ll_row.addView(textView);

            ll_body.addView(ll_row);
        }

        System.out.println("finished");
    }

}